

document.getElementsByClassName("new-game")[0].style.marginBottom='10%';
document.getElementById("dice2").style.display='none';
var leftSideDiv= document.getElementsByClassName("left")[0];
var rightSideDiv= document.getElementsByClassName("right")[0];
var round = 0;
var score = [0,0,0];
var current = [0,0,0];
var currentBox= [0,document.getElementById("current1"),document.getElementById("current2")];
var scoreBox= [0,document.getElementById("score1"),document.getElementById("score2")];
var rolling= false;

document.getElementById("dice").style.visibility='hidden';
document.getElementById("dice2").style.visibility='hidden';

var treshold=100;

changeRound();

function changeRound() {
    switch (round){
        case 0:
            round=1;
            leftSideDiv.style.backgroundColor='#F7F7F7';
            rightSideDiv.style.backgroundColor='#FFFFFF';
            document.getElementById("i1").style.visibility='visible';
            document.getElementById("i2").style.visibility='hidden';

            break;
        case 1:
            round=2;
            leftSideDiv.style.backgroundColor='#FFFFFF';
            rightSideDiv.style.backgroundColor='#F7F7F7';

            document.getElementById("i1").style.visibility='hidden';
            document.getElementById("i2").style.visibility='visible';
            break;
        case 2:
            round=1;
            rightSideDiv.style.backgroundColor='#FFFFFF';
            leftSideDiv.style.backgroundColor='#F7F7F7';

            document.getElementById("i1").style.visibility='visible';
            document.getElementById("i2").style.visibility='hidden';
            break;
    }
}

function rollDice(){
    rolling=true;

    var num = Math.floor(Math.random() * 6) + 1;

    document.getElementById('dice').src='dice/dice-'+(Math.floor(Math.random() * 6) + 1)+'.png';

    setTimeout(function () {
        document.getElementById('dice').src='dice/dice-'+(Math.floor(Math.random() * 6) + 1)+'.png';
        setTimeout(function () {
            document.getElementById('dice').src='dice/dice-'+(Math.floor(Math.random() * 6) + 1)+'.png';
            setTimeout(function () {
                document.getElementById('dice').src='dice/dice-'+num+'.png';
                rolling=false;

            },200)
        },200)
    }, 200);


//        document.getElementById('dice').src='dice/dice-'+num;
    return (num);
}

function setCurrent() {
    changeTreshold();
    if(rolling){
        return;
    }

    document.getElementById("dice").style.visibility='visible';
    document.getElementById("dice2").style.visibility='visible';

    var diceNumber = rollDice();

    setTimeout(function () {
        if(diceNumber === 1){
            currentBox[round].innerHTML=0;
            current[round]=0;
            changeRound();
        }else{
            current[round]+=diceNumber;
        }

        currentBox[round].innerHTML=current[round];

        if(parseInt(scoreBox[round].innerHTML) + current[round] >= treshold){
            winState();
        }
    },600);
}

function hold(){
    scoreBox[round].innerHTML = parseInt(scoreBox[round].innerHTML) + current[round];
    currentBox[round].innerHTML = 0;
    current[round]=0;
    if (scoreBox[round].innerHTML >= treshold){
        winState()
    }else{
        changeRound();
    }

}

function winState() {
    document.getElementById("player"+round).innerHTML="WINNER";
    document.getElementById("player"+round).style.color="#E65049";
    document.getElementById("roll").disabled=true;
    document.getElementById("hold").disabled=true;
    document.getElementById("dice").style.visibility='hidden';
    document.getElementById("dice2").style.visibility='hidden';
}

function init() {
    document.getElementById("player1").innerHTML="PLAYER 1";
    document.getElementById("player2").innerHTML="PLAYER 2";
    document.getElementById("player1").style.color="#BABABA";
    document.getElementById("player2").style.color="#BABABA";

    document.getElementById("player2").style.color="#BABABA";
    document.getElementById("player2").style.color="#BABABA";

    document.getElementById("roll").disabled=false;
    document.getElementById("hold").disabled=false;
    currentBox[1].innerHTML=0;
    currentBox[2].innerHTML=0;
    scoreBox[1].innerHTML=0;
    scoreBox[2].innerHTML=0;
    current=[0,0,0];
    round=0;
    changeRound();
}
function changeTreshold() {
    var x = parseInt(document.getElementById('score').value);
    if(isNaN(x)){
        treshold=100;
    }
    else{
        treshold=x;
    }
}